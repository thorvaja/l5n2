System.register(['@angular/core', '../shared/models/user'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, user_1;
    var UserProfileComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (user_1_1) {
                user_1 = user_1_1;
            }],
        execute: function() {
            UserProfileComponent = (function () {
                function UserProfileComponent() {
                }
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', user_1.User)
                ], UserProfileComponent.prototype, "user", void 0);
                UserProfileComponent = __decorate([
                    core_1.Component({
                        selector: 'user-profile',
                        template: "\n\t\t<div *ngIf=\"user\">\n\t  \t\tSelected user is {{user.name}}\n\t  \t\t<input type=\"text\" class=\"form-control\" [(ngModel)]=\"user.name\"/>\n\t  \t</div>\n\t"
                    }), 
                    __metadata('design:paramtypes', [])
                ], UserProfileComponent);
                return UserProfileComponent;
            }());
            exports_1("UserProfileComponent", UserProfileComponent);
        }
    }
});

//# sourceMappingURL=user-profile.component.js.map
