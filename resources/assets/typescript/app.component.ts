import { Component } from '@angular/core';
import { User } from './shared/models/user';
@Component({
    selector: 'my-app',
    templateUrl:'./resources/assets/typescript/app.component.html',
    styleUrls:['./resources/assets/typescript/app.component.css']
})
export class AppComponent {
	webSiteName: string='Thoriseum';
	users: User[]=[
		{id:1,name:"jaimin"},
		{id:2,name:"mamta"},
		{id:3,name:"parth"}
	];
	activeUser: User;
	selectUser(user){
		this.activeUser=user;
	}
	 onUserCreated(event){
	 	// console.log(event);
	 	this.users.push(event.user);
	 }
}
