///<reference path="../../../typings/index.d.ts"/>
import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent }   from './app.component';
import { UserProfileComponent } from './users/user-profile.component';
@NgModule({
    imports:      [ BrowserModule,FormsModule ],
    declarations: [ AppComponent,UserProfileComponent ],
    bootstrap:    [ AppComponent ]
})
export class AppModule { }



